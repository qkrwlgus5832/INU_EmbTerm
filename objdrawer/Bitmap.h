#pragma once


#include <string>
#include <climits>

#include "Length2d.h"


union _32bpp
{
	struct Channel
	{
		unsigned char a, r, g, b;
	};

	unsigned int color;
	Channel ch;
};

class Bitmap
{
private:

	Length2d l2d;
	_32bpp * buf;

public:

	~Bitmap();
	Bitmap(const char* filename, L2dNumeral width, L2dNumeral height);

	const Length2d& getLength2d() const;
	const _32bpp* getBuf() const;
};
