#include <iostream>
#include <memory.h>

#include "Canvas.h"


using namespace std;


Canvas::~Canvas()
{
}


Canvas::Canvas()
	: Canvas(nullptr, 0, 0, 0)
{
}


Canvas::Canvas(Pixel* buf, L2dNumeral width, L2dNumeral height, L2dNumeral gap)
	: Drawable<Pixel>(buf, width, height, gap)
{
}


Canvas::Canvas(const Canvas& from, const Room2d& r2d)
	: Drawable<Pixel>(NULL, Length2d(), from.gap)
{
	int x = r2d.getX();
	int y = r2d.getY();
	L2dNumeral w = r2d.getW();
	L2dNumeral h = r2d.getH();
	L2dNumeral fw = from.l2d.getW();
	L2dNumeral fh = from.l2d.getH();

	if (w > fw)
	{
		w = fw;
	}
	else if (w < 0) //buggy
	{
		w = 0;
	}
	if (h > fh)
	{
		w = fh;
	}
	else if (h < 0) //buggy
	{
		h = 0;
	}

	if (x >= w)
	{
		x = w - 1;
	}
	else if (x < 0)
	{
		x = 0;
	}
	if (y >= h)
	{
		y = h - 1;
	}
	else if (y < 0)
	{
		y = 0;
	}

	this->l2d = Length2d(w, h); // buggy
	this->buf = from.buf + from.gap * y + x;
}
