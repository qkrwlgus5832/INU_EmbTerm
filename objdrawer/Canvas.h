#pragma once


#include "Drawable.h"
#include "Pixelmap.h"
#include "Room2d.h"


class Canvas : public Drawable<Pixel>
{

public:

	virtual ~Canvas();
	Canvas();
    Canvas(Pixel* buf, L2dNumeral width, L2dNumeral height, L2dNumeral gap=0);
	Canvas(const Canvas& from, const Room2d& r2d);
};
