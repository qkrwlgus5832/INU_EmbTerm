#pragma once


#include "Length2d.h"
#include "Point2d.h"


template<typename TypenamePixel>
class Drawable
{
protected:

	Length2d l2d;
	TypenamePixel * buf;
	L2dNumeral gap;

public:

	virtual ~Drawable<TypenamePixel>() { }
	Drawable(TypenamePixel* buf, const Length2d& l2d, L2dNumeral gap=0);
	Drawable(TypenamePixel* buf, L2dNumeral width, L2dNumeral height, L2dNumeral gap=0);

	const Length2d& getLength2d() const;
	L2dNumeral getGap() const;

	void hlineCoord(TypenamePixel color, L2dNumeral y, L2dNumeral x0, L2dNumeral x1);
	void hlineLen(TypenamePixel color, L2dNumeral y, L2dNumeral x, L2dNumeral length);
	void fillRect(TypenamePixel p, int x0, int y0, int x1, int y1);
	void fill(TypenamePixel p);
	void copyRect(
			int destX, int destY,
			int srcX0, int srcY0, int srcX1, int srcY1);
	void printDrawable(
			int destX, int destY,
			const Drawable<TypenamePixel>& src,
			int srcX0, int srcY0, int srcX1, int srcY1);
	void printDrawable(
			int destX, int destY,
			const Drawable<TypenamePixel>& src,
			int srcX0, int srcY0, int srcX1, int srcY1,
			const Drawable<TypenamePixel>& srcAlpha);
	void printDrawable(const Drawable<TypenamePixel>& src);
	void printDrawable(
			const Drawable<TypenamePixel>& src,
			const Drawable<TypenamePixel>& srcAlpha);
	void printDrawableOR(const Drawable<TypenamePixel>& src);
	void printDrawableOR(int destX, int destY,
			const Drawable<TypenamePixel>& src,
			int srcX0, int srcY0, int srcX1, int srcY1);
	void printDrawable(
			int destX, int destY,
			const Drawable<TypenamePixel>& src,
			int srcX0, int srcY0, int srcX1, int srcY1,
			TypenamePixel color);
	void printDrawable(const Drawable<TypenamePixel>& src, TypenamePixel color);

	virtual TypenamePixel getPixel(const Point2d& pos) const;
};


template<typename TypenamePixel>
Drawable<TypenamePixel>::Drawable(TypenamePixel* buf, const Length2d& l2d, L2dNumeral gap)
	: Drawable<TypenamePixel>(buf, l2d.getW(), l2d.getH(), gap)
{
}


template<typename TypenamePixel>
Drawable<TypenamePixel>::Drawable(TypenamePixel* buf, L2dNumeral width, L2dNumeral height, L2dNumeral gap)
	: buf(buf)
{
	l2d.setW(width);
	l2d.setH(height);
	if (gap < width)
	{
		this->gap = width;
	}
	else
	{
		this->gap = gap;
	}
}


template<typename TypenamePixel>
const Length2d& Drawable<TypenamePixel>::getLength2d() const
{
	return this->l2d;
}


template<typename TypenamePixel>
L2dNumeral Drawable<TypenamePixel>::getGap() const
{
	return this->gap;
}


template<typename TypenamePixel>
void Drawable<TypenamePixel>::hlineCoord(TypenamePixel color, L2dNumeral y, L2dNumeral x0, L2dNumeral x1)
{
	if (y < 0 || this->l2d.getH() <= y)
	{
		return;
	}

	L2dNumeral width = this->l2d.getW();
	if (width == 0)
	{
		return;
	}

	L2dNumeral xmax = width - 1;
	if (x0 > xmax)
	{
		x0 = xmax;
	}
	else if (x0 < 0)
	{
		x0 = 0;
	}
	if (x1 > xmax)
	{
		x1 = xmax;
	}
	else if (x1 < 0)
	{
		x1 = 0;
	}

	if (x0 > x1)
	{
		L2dNumeral i2 = x0;
		x0 = x1;
		x1 = i2;
	}
	TypenamePixel * start = this->buf + y * this->gap;
	for (L2dNumeral i1 = x0; i1 < x1; ++i1)
	{
		start[i1] = color;
	}
}


template<typename TypenamePixel>
void Drawable<TypenamePixel>::hlineLen(TypenamePixel color, L2dNumeral y, L2dNumeral x, L2dNumeral length)
{
	this->hlineCoord(color, y, x, x + length);
}

template<typename TypenamePixel>
void Drawable<TypenamePixel>::fillRect(TypenamePixel p, int x0, int y0, int x1, int y1)
{
	int temp;
	if (x0 > x1)
	{
		temp = x0;
		x0 = x1;
		x1 = temp;
	}
	if (y0 > y1)
	{
		temp = y0;
		y0 = y1;
		y1 = temp;
	}

	L2dNumeral height = this->l2d.getH();
	L2dNumeral width = this->l2d.getW();
	TypenamePixel * curr;
	curr = this->buf + y0 * this->gap;
	for (int yy = y0; yy < y1 && yy < height; ++yy)
	{
		for (int xx = x0; xx < x1 && xx < width; ++xx)
		{
			curr[xx] = p;
		}
		curr += this->gap;
	}
}

template<typename TypenamePixel>
void Drawable<TypenamePixel>::fill(TypenamePixel p)
{
	L2dNumeral height = this->l2d.getH();
	L2dNumeral width = this->l2d.getW();
	TypenamePixel * curr;
	curr = this->buf;
	for (int yy = 0; yy < height; ++yy)
	{
		for (int xx = 0; xx < width; ++xx)
		{
			curr[xx] = p;
		}
		curr += this->gap;
	}
}


template<typename TypenamePixel>
void Drawable<TypenamePixel>::copyRect(
	int destX,
	int destY,
	int srcX0,
	int srcY0,
	int srcX1,
	int srcY1)
{
	int temp;
	if (srcX0 > srcX1)
	{
		temp = srcX0;
		srcX0 = srcX1;
		srcX1 = temp;
	}
	if (srcY0 > srcY1)
	{
		temp = srcY0;
		srcY0 = srcY1;
		srcY1 = temp;
	}

	TypenamePixel * destCurr, * srcCurr;
	destCurr = this->buf + destY * this->gap;
	srcCurr = this->buf + srcY0 * this->gap;
	int i0 = sizeof(*destCurr) * ( srcX1 - srcX0 );
	for (int yy = srcY0; yy < srcY1; ++yy)
	{
		memmove(&destCurr[destX], &srcCurr[srcX0], i0);
		destCurr += this->gap;
		srcCurr += this->gap;
	}
}


template<typename TypenamePixel>
void Drawable<TypenamePixel>::printDrawable(
		int destX, int destY,
		const Drawable<TypenamePixel>& src,
		int srcX0, int srcY0, int srcX1, int srcY1)
{
	const Length2d& srcLength2d = src.getLength2d();
	L2dNumeral srcH = srcLength2d.getH();
	L2dNumeral srcW = srcLength2d.getW();
	L2dNumeral destH = this->l2d.getH();
	L2dNumeral destW = this->l2d.getW();

	if (destX < 0)
	{
		srcX0 -= destX;
		destX = 0;
	}
	else if (destX >= destW)
	{
		return;
	}
	if (destY < 0)
	{
		srcY0 -= destY;
		destY = 0;
	}
	else if (destY >= destH)
	{
		return;
	}
	if (srcX0 < 0)
	{
		destX -= srcX0;
		srcX0 = 0;
	}
	else if (srcY0 < 0)
	{
		destY -= srcY0;
		srcY0 = 0;
	}
	if (srcX1 > srcW)
	{
		srcX1 = srcW;
	}
	if (srcY1 > srcH)
	{
		srcY1 = srcH;
	}

	const TypenamePixel * srcCurr;
	TypenamePixel * destCurr;

	destCurr = this->buf + destY * this->gap;
	srcCurr = src.buf + srcY0 * src.gap;
	for (int sy = srcY0, dy = destY; sy < srcY1 && dy < destH; ++sy, ++dy)
	{
		for (int sx = srcX0, dx = destX; sx < srcX1 && dx < destW; ++sx, ++dx)
		{
			destCurr[dx] = srcCurr[sx];
		}
		destCurr += this->gap;
		srcCurr += src.gap;
	}
}


template<typename TypenamePixel>
void Drawable<TypenamePixel>::printDrawable(
		int destX, int destY,
		const Drawable<TypenamePixel>& src,
		int srcX0, int srcY0, int srcX1, int srcY1,
		const Drawable<TypenamePixel>& srcAlpha)
{
	const Length2d& srcLength2d = src.getLength2d();
	L2dNumeral srcH = srcLength2d.getH();
	L2dNumeral srcW = srcLength2d.getW();
	L2dNumeral destH = this->l2d.getH();
	L2dNumeral destW = this->l2d.getW();

	if (destX < 0)
	{
		srcX0 -= destX;
		destX = 0;
	}
	else if (destX >= destW)
	{
		return;
	}
	if (destY < 0)
	{
		srcY0 -= destY;
		destY = 0;
	}
	else if (destY >= destH)
	{
		return;
	}
	if (srcX0 < 0)
	{
		destX -= srcX0;
		srcX0 = 0;
	}
	else if (srcY0 < 0)
	{
		destY -= srcY0;
		srcY0 = 0;
	}
	if (srcX1 > srcW)
	{
		srcX1 = srcW;
	}
	if (srcY1 > srcH)
	{
		srcY1 = srcH;
	}

	const TypenamePixel * srcCurr;
	const TypenamePixel * srcACurr;
	TypenamePixel * destCurr;

	destCurr = this->buf + destY * this->gap;
	srcCurr = src.buf + srcY0 * src.gap;
	srcACurr = srcAlpha.buf + srcY0 * srcAlpha.gap;
	for (int sy = srcY0, dy = destY; sy < srcY1 && dy < destH; ++sy, ++dy)
	{
		for (int sx = srcX0, dx = destX; sx < srcX1 && dx < destW; ++sx, ++dx)
		{
			destCurr[dx] = ( srcCurr[sx] & srcACurr[sx] ) + ( destCurr[dx] & ~srcACurr[sx] );
		}
		destCurr += this->gap;
		srcCurr += src.gap;
		srcACurr += srcAlpha.gap;
	}
}

template<typename TypenamePixel>
void Drawable<TypenamePixel>::printDrawable(const Drawable<TypenamePixel>& src)
{
	printDrawable(0, 0, src, 0, 0, src.l2d.getW(), src.l2d.getH());
}


template<typename TypenamePixel>
void Drawable<TypenamePixel>::printDrawable(
		const Drawable<TypenamePixel>& src, const Drawable<TypenamePixel>& srcAlpha)
{
	printDrawable(0, 0, src, 0, 0, src.l2d.getW(), src.l2d.getH(), srcAlpha);
}


template<typename TypenamePixel>
void Drawable<TypenamePixel>::printDrawableOR(
	const Drawable<TypenamePixel>& src)
{
	this->printDrawableOR(0, 0, src, 0, 0, src.l2d.getW(), src.l2d.getH());
}


template<typename TypenamePixel>
void Drawable<TypenamePixel>::printDrawableOR(
	int destX, int destY,
	const Drawable<TypenamePixel>& src,
	int srcX0, int srcY0, int srcX1, int srcY1)
{
	const Length2d& srcLength2d = src.getLength2d();
	L2dNumeral srcH = srcLength2d.getH();
	L2dNumeral srcW = srcLength2d.getW();
	L2dNumeral destH = this->l2d.getH();
	L2dNumeral destW = this->l2d.getW();

	if (destX < 0)
	{
		srcX0 -= destX;
		destX = 0;
	}
	else if (destX >= destW)
	{
		return;
	}
	if (destY < 0)
	{
		srcY0 -= destY;
		destY = 0;
	}
	else if (destY >= destH)
	{
		return;
	}
	if (srcX0 < 0)
	{
		destX -= srcX0;
		srcX0 = 0;
	}
	else if (srcY0 < 0)
	{
		destY -= srcY0;
		srcY0 = 0;
	}
	if (srcX1 > srcW)
	{
		srcX1 = srcW;
	}
	if (srcY1 > srcH)
	{
		srcY1 = srcH;
	}

	const TypenamePixel * srcCurr;
	TypenamePixel * destCurr;

	destCurr = this->buf + destY * this->gap;
	srcCurr = src.buf + srcY0 * src.gap;
	for (int sy = srcY0, dy = destY; sy < srcY1 && dy < destH; ++sy, ++dy)
	{
		for (int sx = srcX0, dx = destX; sx < srcX1 && dx < destW; ++sx, ++dx)
		{
			destCurr[dx] |= srcCurr[sx];
		}
		destCurr += this->gap;
		srcCurr += src.gap;
	}
}


template<typename TypenamePixel>
void Drawable<TypenamePixel>::printDrawable(
		int destX, int destY,
		const Drawable<TypenamePixel>& src,
		int srcX0, int srcY0, int srcX1, int srcY1, TypenamePixel color)
{
	const Length2d& srcLength2d = src.getLength2d();
	L2dNumeral srcH = srcLength2d.getH();
	L2dNumeral srcW = srcLength2d.getW();
	L2dNumeral destH = this->l2d.getH();
	L2dNumeral destW = this->l2d.getW();

	if (destX < 0)
	{
		srcX0 -= destX;
		destX = 0;
	}
	else if (destX >= destW)
	{
		return;
	}
	if (destY < 0)
	{
		srcY0 -= destY;
		destY = 0;
	}
	else if (destY >= destH)
	{
		return;
	}
	if (srcX0 < 0)
	{
		destX -= srcX0;
		srcX0 = 0;
	}
	else if (srcY0 < 0)
	{
		destY -= srcY0;
		srcY0 = 0;
	}
	if (srcX1 > srcW)
	{
		srcX1 = srcW;
	}
	if (srcY1 > srcH)
	{
		srcY1 = srcH;
	}

	const TypenamePixel * srcCurr;
	TypenamePixel * destCurr;

	destCurr = this->buf + destY * this->gap;
	srcCurr = src.buf + srcY0 * src.gap;
	for (int sy = srcY0, dy = destY; sy < srcY1 && dy < destH; ++sy, ++dy)
	{
		for (int sx = srcX0, dx = destX; sx < srcX1 && dx < destW; ++sx, ++dx)
		{
			destCurr[dx] = ( srcCurr[sx] & color ) + ( ~srcCurr[sx] & destCurr[dx] );
		}
		destCurr += this->gap;
		srcCurr += src.gap;
	}
}


template<typename TypenamePixel>
void Drawable<TypenamePixel>::printDrawable(const Drawable<TypenamePixel>& src, TypenamePixel color)
{
	printDrawable(0, 0, src, 0, 0, src.l2d.getW(), src.l2d.getH(), color);
}

template<typename TypenamePixel>
TypenamePixel Drawable<TypenamePixel>::getPixel(const Point2d& pos) const
{
	return this->buf[pos.getY() * this->gap + pos.getX()];
}
