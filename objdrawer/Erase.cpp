#include <iostream>
#include "Eraser.h"
using namespace std;


void Eraser::drawline2(Drawable<Pixel>& d, const Point2d& start, const Point2d& next, Pixel color) {
	int xpos1 = start.getX();
	int ypos1 = start.getY();
	int xpos2 = next.getX();
	int ypos2 = next.getY();
	float x2 = (float)xpos1;
	float y2 = (float)ypos1;
	float dx = (xpos2 - xpos1) / 10.f;
	float dy = (ypos2 - ypos1) / 10.f;

	for (int i = 0; i < 10; i++)
	{
		x2 += dx;
		y2 += dy;
		if (x2 < 0 || y2 < 0)
		{
			break;
		}
		for (int i = -3; i < 3; i++) {
			for (int j = -3; j < 3; j++) {
				d.hlineLen(color, (int)y2 + j, (int)x2 + i, 1);
			}
		}
		
	}
}


void Eraser::drawImpl(Drawable<Pixel>& d)
{
	this->color = Pixel_make(255, 255, 255);
	drawline2(d, this->prev.getPoint2d(), this->next.getPoint2d(), color);

}
