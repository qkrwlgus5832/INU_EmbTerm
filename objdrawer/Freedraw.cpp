#include <iostream>
#include "Freedraw.h"

using namespace std;

void Freedraw::drawImpl(Drawable<Pixel>& d)
{
	this->drawline(d, this->prev.getPoint2d(), this->next.getPoint2d(), color);
}
