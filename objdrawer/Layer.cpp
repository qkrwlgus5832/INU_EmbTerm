#include "Layer.h"


Layer::Layer(int x, int y, L2dNumeral w, L2dNumeral h)
	: Layer(Room2d(x, y, w, h))
{
}


Layer::Layer(const Point2d& pt2d, const Length2d& l2d)
	: Pixelmap(l2d), pt2d(pt2d)
{
}


Layer::Layer(const Room2d& r)
	: Layer(r.getPoint2d(), r.getLength2d())
{
}


Point2d& Layer::getPoint2d()
{
	return this->pt2d;
}


const Point2d& Layer::getPoint2d() const
{
	return this->pt2d;
}


void Layer::setColor(Pixel color)
{
	this->color = color;
}


Pixel Layer::getColor() const
{
	return this->color;
}


void Layer::erase(const Layer& eraser)
{
	const Length2d& srcLength2d = eraser.getLength2d();
	L2dNumeral srcX0 = 0;
	L2dNumeral srcY0 = 0;
	L2dNumeral srcX1 = srcLength2d.getW();
	L2dNumeral srcY1 = srcLength2d.getH();
	L2dNumeral srcW = srcLength2d.getW();
	L2dNumeral srcH = srcLength2d.getH();
	L2dNumeral destX = -this->pt2d.getX();
	L2dNumeral destY = -this->pt2d.getY();
	L2dNumeral destH = this->l2d.getH();
	L2dNumeral destW = this->l2d.getW();

	if (destX < 0)
	{
		srcX0 -= destX;
		destX = 0;
	}
	else if (destX >= destW)
	{
		return;
	}
	if (destY < 0)
	{
		srcY0 -= destY;
		destY = 0;
	}
	else if (destY >= destH)
	{
		return;
	}
	if (srcX0 < 0)
	{
		destX -= srcX0;
		srcX0 = 0;
	}
	else if (srcY0 < 0)
	{
		destY -= srcY0;
		srcY0 = 0;
	}
	if (srcX1 > srcW)
	{
		srcX1 = srcW;
	}
	if (srcY1 > srcH)
	{
		srcY1 = srcH;
	}

	const Pixel * srcCurr;
	Pixel * destCurr;

	destCurr = this->buf + destY * this->gap;
	srcCurr = eraser.buf + srcY0 * eraser.gap;
	for (int sy = srcY0, dy = destY; sy < srcY1 && dy < destH; ++sy, ++dy)
	{
		for (int sx = srcX0, dx = destX; sx < srcX1 && dx < destW; ++sx, ++dx)
		{
			destCurr[dx] &= ~srcCurr[sx];
		}
		destCurr += this->gap;
		srcCurr += eraser.gap;
	}
}
