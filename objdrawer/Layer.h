#pragma once


#include "Pixelmap.h"
#include "Room2d.h"


class Layer : public Pixelmap
{
private:

	Point2d pt2d;
	Pixel color;

public:

	Layer(int x=0, int y=0, L2dNumeral w=0, L2dNumeral h=0);
	Layer(const Point2d& pt2d, const Length2d& l2d);
	Layer(const Room2d& r2d);

	Point2d& getPoint2d();
	const Point2d& getPoint2d() const;
	void setColor(Pixel color);
	Pixel getColor() const;
	void erase(const Layer& eraser);
};
