#pragma once


#include <cstddef>


typedef int L2dNumeral;


class Length2d
{
private:

	L2dNumeral w, h;
	long long area;

	void updateArea();

public:

	Length2d(L2dNumeral w=0, L2dNumeral h=0);

	void setWH(L2dNumeral w, L2dNumeral h);
	void setW(L2dNumeral w);
	void setH(L2dNumeral h);
	L2dNumeral getW() const;
	L2dNumeral getH() const;
	long long getArea() const;
};
