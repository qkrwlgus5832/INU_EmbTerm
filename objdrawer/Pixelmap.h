#pragma once


#include "Drawable.h"
#include "Bitmap.h"
#include "Pixel.h"


class Pixelmap : public Drawable<Pixel>
{
public:

	virtual ~Pixelmap();
	Pixelmap(const Pixelmap& pm);
	Pixelmap(const Bitmap& bm);
	Pixelmap(L2dNumeral width, L2dNumeral height);
	Pixelmap(const Length2d& l2d);

	Pixel* getBuf();
	const Pixel* getBuf() const;
	Pixel getPixel(L2dNumeral x, L2dNumeral y) const;
};
