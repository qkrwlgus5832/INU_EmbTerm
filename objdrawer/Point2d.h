#pragma once


class Point2d
{
private:

	int x, y;

public:

	Point2d(int x=0, int y=0);

	void setXY(int x, int y);
	void setX(int x);
	void setY(int y);
	int getX() const;
	int getY() const;
	void setOrigin(const Point2d& origin);
	double getDistance(const Point2d& origin);
};
