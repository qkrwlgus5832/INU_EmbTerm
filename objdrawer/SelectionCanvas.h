﻿#pragma once


#include "SelectionHelper.h"
#include "Canvas.h"


class SelectionCanvas : public Canvas
{
private:

	SelectionHelper sh;

public:

	~SelectionCanvas();
	SelectionCanvas();
	SelectionCanvas(const Canvas& c);
	SelectionCanvas(const Canvas& c, const Room2d& r2d);

	SelectionHelper& getSelectionHelper();
};
