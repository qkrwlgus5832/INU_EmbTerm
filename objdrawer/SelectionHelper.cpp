#include <memory.h>

#include "SelectionHelper.h"


SelectionHelper::~SelectionHelper()
{
}


SelectionHelper::SelectionHelper(const Length2d& l2d)
	: Drawable<Layer*>(nullptr, l2d, 0)
{
	size_t area = (size_t)l2d.getArea();
	this->buf = new Layer*[area];
	for (size_t i1 = 0; i1 < area; ++i1)
	{
		this->buf[i1] = nullptr;
	}
}


void SelectionHelper::overwriteLayerPtr(const SelectionHelper& sh)
{
	const Length2d& srcLength2d = sh.getLength2d();
	L2dNumeral srcH = srcLength2d.getH();
	L2dNumeral srcW = srcLength2d.getW();
	L2dNumeral destH = this->l2d.getH();
	L2dNumeral destW = this->l2d.getW();
	L2dNumeral destX = 0;
	L2dNumeral destY = 0;
	L2dNumeral srcX0 = 0;
	L2dNumeral srcY0 = 0;
	L2dNumeral srcX1 = srcW;
	L2dNumeral srcY1 = srcH;

	if (destX < 0)
	{
		srcX0 -= destX;
		destX = 0;
	}
	else if (destX >= destW)
	{
		return;
	}
	if (destY < 0)
	{
		srcY0 -= destY;
		destY = 0;
	}
	else if (destY >= destH)
	{
		return;
	}
	if (srcX0 < 0)
	{
		destX -= srcX0;
		srcX0 = 0;
	}
	else if (srcY0 < 0)
	{
		destY -= srcY0;
		srcY0 = 0;
	}
	if (srcX1 > srcW)
	{
		srcX1 = srcW;
	}
	if (srcY1 > srcH)
	{
		srcY1 = srcH;
	}

	Layer ** destCurr;
	Layer *const* srcCurr;

	destCurr = this->buf + destY * this->gap;
	srcCurr = sh.buf + srcY0 * sh.gap;
	for (int sy = srcY0, dy = destY; sy < srcY1 && dy < destH; ++sy, ++dy)
	{
		for (int sx = srcX0, dx = destX; sx < srcX1 && dx < destW; ++sx, ++dx)
		{
			if (srcCurr[sx])
			{
				destCurr[dx] = srcCurr[sx];
			}
		}
		destCurr += this->gap;
		srcCurr += sh.gap;
	}
}


void SelectionHelper::overwriteLayerPtr(Layer& l)
{
	this->overwriteLayerPtr(l, &l);
}


void SelectionHelper::overwriteLayerPtr(Layer& l, Layer* pl)
{
	const Point2d& srcPoint2d = l.getPoint2d();
	const Length2d& srcLength2d = l.getLength2d();
	L2dNumeral srcH = srcLength2d.getH();
	L2dNumeral srcW = srcLength2d.getW();
	L2dNumeral destH = this->l2d.getH();
	L2dNumeral destW = this->l2d.getW();
	L2dNumeral destX = srcPoint2d.getX();
	L2dNumeral destY = srcPoint2d.getY();
	L2dNumeral srcX0 = 0;
	L2dNumeral srcY0 = 0;
	L2dNumeral srcX1 = srcW;
	L2dNumeral srcY1 = srcH;

	if (destX < 0)
	{
		srcX0 -= destX;
		destX = 0;
	}
	else if (destX >= destW)
	{
		return;
	}
	if (destY < 0)
	{
		srcY0 -= destY;
		destY = 0;
	}
	else if (destY >= destH)
	{
		return;
	}
	if (srcX0 < 0)
	{
		destX -= srcX0;
		srcX0 = 0;
	}
	else if (srcY0 < 0)
	{
		destY -= srcY0;
		srcY0 = 0;
	}
	if (srcX1 > srcW)
	{
		srcX1 = srcW;
	}
	if (srcY1 > srcH)
	{
		srcY1 = srcH;
	}

	Layer ** destCurr;

	destCurr = this->buf + destY * this->gap;
	for (int sy = srcY0, dy = destY; sy < srcY1 && dy < destH; ++sy, ++dy)
	{
		for (int sx = srcX0, dx = destX; sx < srcX1 && dx < destW; ++sx, ++dx)
		{
			if (l.getPixel(sx, sy))
			{
				destCurr[dx] = pl;
			}
		}
		destCurr += this->gap;
	}
}
