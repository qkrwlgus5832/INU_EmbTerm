#pragma once


#include "SelectionHelper.h"
#include "Pixelmap.h"


class SelectionPixelmap : public Pixelmap
{
private:

	SelectionHelper sh;

public:

	SelectionPixelmap(const Pixelmap& pm);
	SelectionPixelmap(const Bitmap& bm);
	SelectionPixelmap(L2dNumeral width, L2dNumeral height);
	SelectionPixelmap(const Length2d& l2d);

	SelectionHelper& getSelectionHelper();
};
