#include "TouchEvent.h"


TouchEvent::TouchEvent(const Point2d& pt2d, int pressure)
	: TouchEvent(pt2d.getX(), pt2d.getY(), pressure)
{
	this->setPoint2d(pt2d);
}

TouchEvent::TouchEvent(int x, int y, int pressure)
	: pressure(pressure)
{
	setXY(x, y);
}


const Point2d& TouchEvent::getPoint2d() const
{
	return *this;
}


Point2d& TouchEvent::getPoint2d()
{
	return *this;
}


void TouchEvent::setPoint2d(const Point2d& pt2d)
{
	this->setXY(pt2d.getX(), pt2d.getY());
}


int TouchEvent::getPressure() const
{
	return this->pressure;
}


void TouchEvent::setPressure(int pressure)
{
	this->pressure = pressure;
}
