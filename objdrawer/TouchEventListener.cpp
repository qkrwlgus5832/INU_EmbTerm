#include <iostream>
#include <string>

#include "TouchEventListener.h"
#include "Line.h"
#include "Rectangle.h"
#include "Oval.h"
#include "Freedraw.h"


using namespace std;


TouchEventListener::TouchEventListener(
		const Room2d& r2d, const std::function<void(const TouchEvent&)>& action)
	: r2d(r2d), action(action)
{
}


bool TouchEventListener::listenTo(const TouchEvent& te)
{
	bool result = this->r2d.covers(te.getPoint2d());

	if (result)
	{
		TouchEvent moved(te);
		moved.getPoint2d().setOrigin(this->r2d.getPoint2d());
		this->action(moved);
	}

	return result;
}
