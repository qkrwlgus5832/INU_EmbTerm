#pragma once


#include <vector>
#include <linux/fb.h> /*for fb_var_screeninfo, FBIOGET_VSCREENINFO*/

#include "Screen.h"
#include "TouchEventListener.h"


class TouchScreen : public Screen
{
private:

	std::vector<TouchEventListener> telVector;
	TouchEvent te;
	double A, B, C, D, E, F, K;
	int eventFileDescr;
	int rawX = INT_MIN, rawY = INT_MIN;

public:

	~TouchScreen();
	TouchScreen(const char* fbFileName, const char* eventFileName);

	TouchEvent waitForTouchEvent();
	std::vector<TouchEventListener>& getTouchEventListenerVector();
	TouchEvent wakeTouchEventListner();
	void wakeTouchEventListner(const TouchEvent& te);
};
