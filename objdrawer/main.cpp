#include <iostream>
#include <vector>
#include <list>
#include <stdlib.h>/* for exit */
#include <unistd.h> /* for open/close.. */
#include <fcntl.h> /* for O_RDONLY */
#include <sys/ioctl.h> /* for ioctl*/
#include <sys/types.h> /* for lseek() */
#include <sys/mman.h> /* for mmap and munmap */
#include <linux/fb.h> /*for fb_var_screeninfo, FBIOGET_VSCREENINFO*/

//#include "../include/customdef.h"
#include "TouchScreen.h"
#include "Layer.h"
#include "Rectangle.h"
#include "Line.h"
#include "Oval.h"
#include "Freedraw.h"
#include "Eraser.h"
#include "SelectionCanvas.h"
#include "SelectionPixelmap.h"


using namespace std;


void printTouchEvent(const TouchEvent& te)
{
	Point2d pt2d(te.getPoint2d());
	cout
			<< pt2d.getX() << " "
			<< pt2d.getY() << " "
			<< te.getPressure()
			<< endl;
}


int main(int argc, char** argv)
{
	const char* temp;
	if (argc < 2)
	{
		temp = "/dev/input/event4";
	}
	else
	{
		temp = argv[1];
	}
	TouchScreen ts("/dev/fb2", temp);
	Canvas entireScreenCanvas(ts.getCanvas());
	Bitmap uiBitmap("../ui.txt", 320U, 240U);
	entireScreenCanvas.printDrawable(Pixelmap(uiBitmap));

	Rectangle rectangle;
	Oval oval;
	Line line;
	Freedraw freedraw;
	Eraser eraser;
	Rectangle rectangleA;
	Oval ovalA;
	Line lineA;
	Freedraw freedrawA;
	Eraser eraserA;

	SelectionCanvas drawingCanvas(ts.getCanvas(), Room2d(55, 5, 195, 229));
	SelectionHelper& drawingCanvasSH = drawingCanvas.getSelectionHelper();
	SelectionPixelmap top(drawingCanvas.getLength2d());
	SelectionPixelmap topAlpha(top.getLength2d());
	SelectionPixelmap bottom(drawingCanvas.getLength2d());
	SelectionPixelmap drawingBuffer(drawingCanvas.getLength2d());
	Shape * shape = &freedraw;
	Shape * shapeA = &freedrawA;
	list<Layer*> layerList;
	Layer * tempLayer = nullptr;
	Pixel color = Pixel_make(0, 0, 0);
	bool fill = false;
	Layer * selected = nullptr;
	Point2d from;

	vector<TouchEventListener>& telVector = ts.getTouchEventListenerVector();
	telVector =
	{
		TouchEventListener(
				Room2d(6, 17, 40, 23),
				[&](const TouchEvent& te) { shape = &line; shapeA = &lineA; }),
		TouchEventListener(
				Room2d(6, 51, 41, 21),
				[&](const TouchEvent& te) { shape = &rectangle; shapeA = &rectangleA; }),
		TouchEventListener(
				Room2d(6, 82, 41, 20),
				[&](const TouchEvent& te) { shape = &oval; shapeA = &ovalA; }),
		TouchEventListener(
				Room2d(7, 112, 39, 18),
				[&](const TouchEvent& te) { shape = &freedraw; shapeA = &freedrawA; }),

		TouchEventListener(
				Room2d(8, 141, 40, 22),
				[&](const TouchEvent& te) { shape = nullptr; shapeA = nullptr; }),
		TouchEventListener(
				Room2d(7, 175, 43, 22),
				[&](const TouchEvent& te) { shape = &eraser; shapeA = &eraserA; }),
		TouchEventListener(
				Room2d(8, 205, 41, 22),
				[&](const TouchEvent& te)
				{
					drawingCanvas.fill(0xffff);
					for (auto var : layerList)
					{
						delete var;
					}
					layerList.clear();
				}),

		TouchEventListener(
				Room2d(254, 9, 30, 21),
				[&](const TouchEvent& te) { color = Pixel_make(255,255,255); }),
		TouchEventListener(
				Room2d(289, 9, 30, 23),
				[&](const TouchEvent& te) { color = Pixel_make(255,201,14); }),
		TouchEventListener(
				Room2d(255, 40, 28 , 25),
				[&](const TouchEvent& te) { color = Pixel_make(237,28,36); }),
		TouchEventListener(
				Room2d(290, 40, 28, 27),
				[&](const TouchEvent& te) { color = Pixel_make(181,230,29); }),
		TouchEventListener(
				Room2d(255, 75, 31, 26),
				[&](const TouchEvent& te) { color = Pixel_make(255,242,0); }),
		TouchEventListener(
				Room2d(291, 75, 29, 27),
				[&](const TouchEvent& te) { color = Pixel_make(9,13,89); }),
		TouchEventListener(
				Room2d(254, 110, 31, 26),
				[&](const TouchEvent& te) { color = Pixel_make(0,162,232); }),
		TouchEventListener(
				Room2d(290, 110, 30, 27),
				[&](const TouchEvent& te) { color = Pixel_make(0,0,0); }),

		TouchEventListener(
				Room2d(261, 152,42 , 23),
				[&](const TouchEvent& te) { fill = false; }),
		TouchEventListener(
				Room2d(262, 190, 42, 21),
				[&](const TouchEvent& te) { fill = true; }),
	

		TouchEventListener(
				Room2d(55, 5, 195, 229),
				[&](const TouchEvent& te)
				{
					if (shape) // drawing
					{
						if (!tempLayer)
						{
							tempLayer = new Layer(Room2d(Point2d(), drawingCanvas.getLength2d()));
							tempLayer->fill(0);
						}

						shape->draw(drawingCanvas, te, color, fill);
						shapeA->draw(*tempLayer, te, 0xffff, fill);
						tempLayer->setColor(color);

						if (shape == &eraser)
						{
							if (te.getPressure() == 0)
							{
								drawingCanvasSH.overwriteLayerPtr(*tempLayer, nullptr);

								for (auto var : layerList)
								{
									var->erase(*tempLayer);
								}
								tempLayer->fill(0);
							}
						}
						else
						{
							if (te.getPressure() == 0)
							{
								drawingCanvasSH.overwriteLayerPtr(*tempLayer);
								layerList.push_back(tempLayer);
								tempLayer = nullptr;
							}
						}
					}
					else // selecting layer
					{
						if (te.getPressure() > 0)
						{
							if (selected)
							{
								selected->getPoint2d().setOrigin(Point2d(from.getX() - te.getX(), from.getY() - te.getY()));
								from.setXY(te.getX(), te.getY());

								drawingBuffer.printDrawable(bottom);
								Point2d pt2d = selected->getPoint2d();
								Length2d l2d = selected->getLength2d();
								drawingBuffer.printDrawable(
											pt2d.getX(), pt2d.getY(),
											*selected, 0, 0, l2d.getW(), l2d.getH(), selected->getColor());
								drawingBuffer.printDrawable(top, topAlpha);

								drawingCanvas.printDrawable(drawingBuffer);
								drawingCanvasSH.fill(nullptr);
								drawingCanvasSH.overwriteLayerPtr(bottom.getSelectionHelper());
								drawingCanvasSH.overwriteLayerPtr(*selected);
								drawingCanvasSH.overwriteLayerPtr(top.getSelectionHelper());
							}
							else
							{
								Layer * layer = drawingCanvasSH.getPixel(te);
								if (layer)
								{
									from.setXY(te.getX(), te.getY());
									auto it = layerList.cbegin();
									bottom.fill(0xffff);
									bottom.getSelectionHelper().fill(nullptr);
									while (it != layerList.cend() && *it != layer)
									{
										Point2d pt2d = (*it)->getPoint2d();
										Length2d l2d = (*it)->getLength2d();
										bottom.printDrawable(
												pt2d.getX(), pt2d.getY(),
												**it, 0, 0, l2d.getW(), l2d.getH(), (*it)->getColor());
										bottom.getSelectionHelper().overwriteLayerPtr(**it);
										++it;
									}
									if (it != layerList.cend())
									{
										++it;
									}
									topAlpha.fill(0);
									top.getSelectionHelper().fill(nullptr);
									while (it != layerList.cend())
									{
										Point2d pt2d = (*it)->getPoint2d();
										Length2d l2d = (*it)->getLength2d();
										top.printDrawable(
												pt2d.getX(), pt2d.getY(),
												**it, 0, 0, l2d.getW(), l2d.getH(), (*it)->getColor());
										top.getSelectionHelper().overwriteLayerPtr(**it);
										topAlpha.printDrawableOR(
												pt2d.getX(), pt2d.getY(),
												**it, 0, 0, l2d.getW(), l2d.getH());
										++it;
									}

									selected = layer;
								}
							}
						}
						else
						{
							selected = nullptr;
						}
					}
				}),
	};
	while (1)
	{
		ts.wakeTouchEventListner();
	}

	return 0;
}
